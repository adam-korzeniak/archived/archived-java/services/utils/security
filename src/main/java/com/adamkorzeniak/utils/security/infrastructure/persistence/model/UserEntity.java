package com.adamkorzeniak.utils.security.infrastructure.persistence.model;

import com.adamkorzeniak.utils.security.infrastructure.model.Role;
import lombok.Data;
import lombok.Generated;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Generated
@Entity
@Table(name = "user", catalog = "authentication")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String username;

    @NotBlank
    @ToString.Exclude
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('ADMIN', 'USER')", nullable = false)
    private Role role;

    private boolean enabled;
}
