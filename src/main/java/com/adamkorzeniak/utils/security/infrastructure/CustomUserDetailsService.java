package com.adamkorzeniak.utils.security.infrastructure;

import com.adamkorzeniak.utils.security.infrastructure.persistence.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CustomUserDetailsService implements UserDetailsService {

    private static final String USERNAME_NOT_FOUND_MESSAGE = "Username %s not found";

    private final UserProvider userProvider;
    private final UserMapper userMapper;

    @Override
    public User loadUserByUsername(String username) {
        com.adamkorzeniak.utils.security.infrastructure.model.User user = userProvider.findByUsername(username)
                .map(userMapper::map)
                .orElseThrow(() -> new UsernameNotFoundException(String.format(USERNAME_NOT_FOUND_MESSAGE, username)));
        return new User(user.getUsername(), user.getPassword(), user.isEnabled(),
                true, true, true, getGrantedAuthorities(user));
    }

    private List<GrantedAuthority> getGrantedAuthorities(com.adamkorzeniak.utils.security.infrastructure.model.User user) {
        var role = new SimpleGrantedAuthority(user.getRole().toGrantedAuthority());
        return Collections.singletonList(role);
    }
}
