package com.adamkorzeniak.utils.security.infrastructure.model;

public enum Role {
    USER, ADMIN;

    public static final String ROLE_PREFIX = "ROLE_";

    public String toGrantedAuthority() {
        return ROLE_PREFIX + super.toString();
    }
}
