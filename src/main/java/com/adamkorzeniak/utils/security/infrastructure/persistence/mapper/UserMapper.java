package com.adamkorzeniak.utils.security.infrastructure.persistence.mapper;

import com.adamkorzeniak.utils.security.infrastructure.model.User;
import com.adamkorzeniak.utils.security.infrastructure.persistence.model.UserEntity;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {
    User map(UserEntity userEntity);
}
