package com.adamkorzeniak.utils.security.infrastructure;

import com.adamkorzeniak.utils.security.infrastructure.persistence.UserRepository;
import com.adamkorzeniak.utils.security.infrastructure.persistence.model.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserProvider {

    private final UserRepository userRepository;

    public Optional<UserEntity> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
