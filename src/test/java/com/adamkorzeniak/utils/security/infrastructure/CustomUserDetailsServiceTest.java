package com.adamkorzeniak.utils.security.infrastructure;

import com.adamkorzeniak.utils.security.infrastructure.persistence.UserRepository;
import com.adamkorzeniak.utils.security.infrastructure.persistence.model.UserEntity;
import com.adamkorzeniak.utils.security.test.utils.Messages;
import com.adamkorzeniak.utils.security.test.utils.TestData;
import com.adamkorzeniak.utils.security.test.utils.builder.UserBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class CustomUserDetailsServiceTest {

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @MockBean
    private UserRepository userRepository;

    @Test
    void LoadUser_UsernameExists_ReturnsUser() {
        UserEntity user = UserBuilder.sample().password(TestData.ENCODED_PASSWORD).enabled(false).build();

        when(userRepository.findByUsername(TestData.ADMIN_USERNAME)).thenReturn(Optional.of(user));

        UserDetails userDetails = userDetailsService.loadUserByUsername(TestData.ADMIN_USERNAME);

        assertThat(userDetails.getUsername()).isEqualTo(TestData.ADMIN_USERNAME);
        assertThat(userDetails.getPassword()).isEqualTo(TestData.ENCODED_PASSWORD);
        assertThat(userDetails.isEnabled()).isFalse();
        assertThat(userDetails.isAccountNonExpired()).isTrue();
        assertThat(userDetails.isAccountNonLocked()).isTrue();
        assertThat(userDetails.isCredentialsNonExpired()).isTrue();

        var authorities = userDetails.getAuthorities();
        assertThat(authorities).hasSize(1);
        var role = authorities.stream().findFirst();
        assertThat(role).isPresent();
        assertThat(role.get().getAuthority()).isEqualTo(TestData.ADMIN_GRANTED_AUTHORITY);
    }

    @Test
    void LoadUser_UsernameNotExists_ThrowsException() {
        when(userRepository.findByUsername(TestData.ADMIN_USERNAME)).thenReturn(Optional.empty());

        var exc = assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername(TestData.ADMIN_USERNAME));
        assertThat(exc.getMessage()).isEqualTo(Messages.ADMIN_USER_NOT_FOUND_MESSAGE);

    }
}
