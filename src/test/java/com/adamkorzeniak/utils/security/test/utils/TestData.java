package com.adamkorzeniak.utils.security.test.utils;

public class TestData {
    public static final String ADMIN_USERNAME = "admin";
    public static final String USER_USERNAME = "user";
    public static final String PASSWORD = "password";
    public static final String ENCODED_PASSWORD = "pa$$W0rd";
    public static final String ADMIN_GRANTED_AUTHORITY = "ROLE_ADMIN";
}
